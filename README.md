# League Application

This repository contains the codebase for the League Application project, which manages seasons, days, matches, and teams for a sports league.

## Tools Installation

### Prerequisites

Before you start, ensure that you have an Integrated Development Environment (IDE) for Java installed. We recommend IntelliJ IDEA, but you can use any IDE of your choice.

### Java and Maven Installation

#### For Windows, Linux, and macOS

1. Download and install the latest version of [Java Development Kit (JDK)](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html) for your operating system.

2. Download and install [Apache Maven](https://maven.apache.org/download.cgi) by following the installation instructions provided on the website.

3. Verify the installation of Java and Maven by running the following commands in your terminal or command prompt:
    ```shell
    java -version
    mvn -v
    ```

## Project Setup

1. Clone this repository to your local machine using Git:
    ```shell
    git clone https://gitlab.com/learningLobster/2jeed.git
    ```

2. Open the project in your preferred IDE.

3. Make sure your IDE is configured to use JDK and Maven for building the project.

## Running the Application

To run the League Application, follow these steps:

1. Navigate to the root directory of the project.

2. Build the project using Maven:
    ```shell
    mvn clean install
    ```

3. Run the application using the Spring Boot Maven plugin:
    ```shell
    mvn spring-boot:run
    ```

4. Once the application is running, you can access it at `http://localhost:8080`.

## Contributing

Contributions to the League Application are welcome! If you find any bugs or have suggestions for improvements, please open an issue or submit a pull request on GitLab.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
